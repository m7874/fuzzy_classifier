
# Construcciones de funciones de membresia difusa
def singleton(e, pts):
    if e == pts[0]:
        return 1.0
    else:
        return 0.0

def triangular(e, pts):
    if e < pts[0] or e > pts[2]:
        return 0.0
    elif e <= pts[1]:
        return (e-pts[0]) * 1.0/(pts[1]-pts[0])
    else:
        return 1.0 - ((e-pts[1]) * 1.0/(pts[2]-pts[1]))

def trapezoid(e, pts):
    if e < pts[0] or e > pts[3]:
        return 0.0
    elif e <= pts[1]:
        return (e-pts[0]) * 1.0/(pts[1]-pts[0])
    elif e >= pts[2]:
        return 1.0 - ((e-pts[2]) * 1.0/(pts[3]-pts[2]))
    else:
        return 1.0

def trapezoid_left(e, pts):
    if e <= pts[0]:
        return 1.0
    elif e <= pts[1]:
        return 1.0 - (e-pts[0]) * 1.0/(pts[1]-pts[0])
    else:
        return 0.0

def trapezoid_right(e, pts):
    if e < pts[0]:
        return 0.0
    elif e <= pts[1]:
        return (e-pts[0]) * 1.0/(pts[1]-pts[0])
    else:
        return 1.0
