from ling_vars import *
from norms import t_norm_min

#%% Variables Difusas

v1 = voltage(0.98)
f1 = frequency(50.04)

v1.plot()
f1.plot()

#%% Reglas Difusas

state = {
    'seguro': [],
    'alerta': [],
    'emergencia': []
}

# Si Tension Alta y Frecuencia Alta Entonces Estado Emergencia
state['emergencia'].append(t_norm_min(v1.alta,f1.alta))

# Si Tension Alta y Frecuencia Media Entonces Estado Alerta
state['alerta'].append(t_norm_min(v1.alta,f1.media))

# Si Tension Alta y Frecuencia Baja Entonces Estado Emergencia
state['alerta'].append(t_norm_min(v1.alta,f1.baja))

# Si Tension Media y Frecuencia Alta Entonces Estado Emergencia
state['emergencia'].append(t_norm_min(v1.media,f1.alta))

# Si Tension Media y Frecuencia Media Entonces Estado Seguro
state['seguro'].append(t_norm_min(v1.media,f1.media))

# Si Tension Media y Frecuencia Baja Entonces Estado Emergencia
state['emergencia'].append(t_norm_min(v1.media,f1.baja))

# Si Tension Baja y Frecuencia Alta Entonces Estado Emergencia
state['emergencia'].append(t_norm_min(v1.baja,f1.alta))

# Si Tension Baja y Frecuencia Media Entonces Estado Alerta
state['alerta'].append(t_norm_min(v1.baja,f1.media))

# Si Tension Baja y Frecuencia Baja Entonces Estado Emergencia
state['emergencia'].append(t_norm_min(v1.baja,f1.baja))

state_agregado = {
    'seguro': max(state['seguro']),
    'alerta': max(state['alerta']),
    'emergencia': max(state['emergencia'])
}

#%% Resultados

print('----------\nTensión -> \tAlta: {}\tMedia: {}\tBaja: {}\n'.format(v1.alta,v1.media,v1.baja))
print('----------\nFrecuencia -> \tAlta: {}\tMedia: {}\tBaja: {}\n'.format(f1.alta,f1.media,f1.baja))
print('----------\n{}\n'.format(state))
print('----------\nEstado Seguro: {}\n'.format(state_agregado['seguro']))
print('----------\nEstado Alerta: {}\n'.format(state_agregado['alerta']))
print('----------\nEstado Emergencia: {}\n'.format(state_agregado['emergencia']))
print('----------\nEstado de la red: {}\n'.format(max(state_agregado, key=state_agregado.get).capitalize()))
