import membership_functions as mbf
import matplotlib.pyplot as plt
import numpy as np

class voltage:

    def __init__(self, value, mbfshape = 'trapezoid'):
        self.value = value
        self.mbfshape = mbfshape

        if self.mbfshape == 'trapezoid':
            self.bx1 = 0.95
            self.bx2 = 0.99
            self.baja = mbf.trapezoid_left(value, [self.bx1, self.bx2])

            self.mx1 = 0.95
            self.mx2 = 0.98
            self.mx3 = 1.02
            self.mx4 = 1.05
            self.media = mbf.trapezoid(value, [self.mx1, self.mx2, self.mx3, self.mx4])

            self.ax1 = 1.01
            self.ax2 = 1.05
            self.alta = mbf.trapezoid_right(value, [self.ax1, self.ax2])


    def plot(self):

        fig, ax = plt.subplots(figsize=(8, 4))

        # baja
        coord = [[0.0,0.0], [0.0,1.0], [self.bx1,1.0], [self.bx2,0.0]]
        coord.append(coord[0]) #repeat the first point to create a 'closed loop'
        xs, ys = zip(*coord) #create lists of x and y values
        ax.plot(xs,ys) 

        # media
        coord = [[self.mx1,0.0], [self.mx2,1.0], [self.mx3,1.0], [self.mx4,0.0],]
        coord.append(coord[0]) #repeat the first point to create a 'closed loop'
        xs, ys = zip(*coord) #create lists of x and y values
        ax.plot(xs,ys) 

        # alta
        coord = [[self.ax1,0.0], [self.ax2,1.0], [2.0,1.0], [2.0,0.0]]
        coord.append(coord[0]) #repeat the first point to create a 'closed loop'
        xs, ys = zip(*coord) #create lists of x and y values
        ax.plot(xs,ys) 

        ax.set_xlim(self.bx1-0.1,self.ax2+0.1)
        xlim = ax.get_xlim()

        mticks = []
        plt.vlines(x=self.value, ymin=0.0, ymax=max(self.baja,self.media,self.alta), linestyle='--', linewidth=2, alpha=0.5)
        if self.baja > 0.0: 
            plt.hlines(y=self.baja, xmin=xlim[0], xmax=self.value, linestyle='--', linewidth=2, alpha=0.5)
            mticks.append(np.round(self.baja,2))
        if self.media > 0.0: 
            plt.hlines(y=self.media, xmin=xlim[0], xmax=self.value, linestyle='--', linewidth=2, alpha=0.5)
            mticks.append(np.round(self.media,2))
        if self.alta > 0.0: 
            plt.hlines(y=self.alta, xmin=xlim[0], xmax=self.value, linestyle='--', linewidth=2, alpha=0.5)
            mticks.append(np.round(self.alta,2))

        ax.set_xlim(xlim)
        ax.set_ylim(0.0,1.2)

        plt.grid()
        plt.show()


class frequency:

    def __init__(self, value, mbfshape = 'trapezoid'):
        self.value = value
        self.mbfshape = mbfshape

        if self.mbfshape == 'trapezoid':
            self.bx1 = 49.8
            self.bx2 = 49.97
            self.baja = mbf.trapezoid_left(value, [self.bx1, self.bx2])

            self.mx1 = 49.95
            self.mx2 = 49.98
            self.mx3 = 50.02
            self.mx4 = 50.05
            self.media = mbf.trapezoid(value, [self.mx1, self.mx2, self.mx3, self.mx4])

            self.ax1 = 50.03
            self.ax2 = 50.2
            self.alta = mbf.trapezoid_right(value, [self.ax1, self.ax2])


    def plot(self):

        fig, ax = plt.subplots(figsize=(8, 4))

        # baja
        coord = [[0.0,0.0], [0.0,1.0], [self.bx1,1.0], [self.bx2,0.0]]
        coord.append(coord[0]) #repeat the first point to create a 'closed loop'
        xs, ys = zip(*coord) #create lists of x and y values
        ax.plot(xs,ys) 

        # media
        coord = [[self.mx1,0.0], [self.mx2,1.0], [self.mx3,1.0], [self.mx4,0.0],]
        coord.append(coord[0]) #repeat the first point to create a 'closed loop'
        xs, ys = zip(*coord) #create lists of x and y values
        ax.plot(xs,ys) 

        # alta
        coord = [[self.ax1,0.0], [self.ax2,1.0], [70.0,1.0], [70.0,0.0]]
        coord.append(coord[0]) #repeat the first point to create a 'closed loop'
        xs, ys = zip(*coord) #create lists of x and y values
        ax.plot(xs,ys) 

        ax.set_xlim(self.bx1-0.1,self.ax2+0.1)
        xlim = ax.get_xlim()

        mticks = []
        plt.vlines(x=self.value, ymin=0.0, ymax=max(self.baja,self.media,self.alta), linestyle='--', linewidth=2, alpha=0.5)
        if self.baja > 0.0: 
            plt.hlines(y=self.baja, xmin=xlim[0], xmax=self.value, linestyle='--', linewidth=2, alpha=0.5)
            mticks.append(np.round(self.baja,2))
        if self.media > 0.0: 
            plt.hlines(y=self.media, xmin=xlim[0], xmax=self.value, linestyle='--', linewidth=2, alpha=0.5)
            mticks.append(np.round(self.media,2))
        if self.alta > 0.0: 
            plt.hlines(y=self.alta, xmin=xlim[0], xmax=self.value, linestyle='--', linewidth=2, alpha=0.5)
            mticks.append(np.round(self.alta,2))

        ax.set_xlim(xlim)
        ax.set_ylim(0.0,1.2)

        plt.grid()
        plt.show()

    
